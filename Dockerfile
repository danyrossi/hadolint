FROM ubuntu:20.04
WORKDIR /src
ADD https://github.com/hadolint/hadolint/releases/download/v2.4.0/hadolint-Linux-x86_64 /src
RUN mv /src/hadolint-Linux-x86_64 /usr/local/bin/hadolint
RUN chmod +x /usr/local/bin/hadolint
